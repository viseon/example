import ch.viseon.openOrca.server.OrcaSource
import ch.viseon.openOrca.server.servlet.DefaultServlet
import ch.viseon.openOrca.share.*
import io.reactivex.Observable
import javax.servlet.annotation.WebServlet

/**
 * Created by sandro on 11.05.17.
 */

@WebServlet(name = "OrcaServlet", value = "/orca")
class OrcaServlet : DefaultServlet(ch.viseon.openOrca.server.servlet.Configuration("http://localhost:63342")) {

  private val rowModelType = ModelType("rowData")

  private fun rowModelBuilder(modelId: ModelId): PresentationModelBuilder {
    return PresentationModelBuilder(modelId, rowModelType) {
      property(PropertyName("Name")) {
        value = StringValue("Name of $modelId")
        label = LabelPropertyValue("Name")
      }
      property(PropertyName("Lastname")) {
        value = StringValue("Last name of $modelId")
        label = LabelPropertyValue("Last name")
      }
    }
  }

  override fun processRequest(orcaSource: OrcaSource): Observable<CommandData> {
    val modelId = ModelId("Model1")
    val propertyName = PropertyName("Prop1")

    return orcaSource.observeProperty(modelId, propertyName)
        .map { evt -> evt.newValue as StringValue }
        .map { (string) -> string }
        .filter { it.isNotBlank() }
        .map { value -> value.toInt() }
        .flatMap {
          Observable.concat<CommandData>(Observable.just(RemoveModelByTypeCommandData(Source.RESPONSE, rowModelType)),
              Observable.fromIterable<CommandData>(
                  IntRange(1, it)
                      .map { rowModelBuilder(ModelId(it.toString())).build(Source.RESPONSE) }))
        }
  }

}

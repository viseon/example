import {run} from '@cycle/run';
import {makeDOMDriver, DOMSource, h2, div, h1, a, h3, input, p, li, ul} from '@cycle/dom';
import xs, {Stream} from "xstream";

import {ch,} from "openorca-js-client"

import ModelId = ch.viseon.openOrca.share.ModelId
import ModelType = ch.viseon.openOrca.share.ModelType;
import PropertyName = ch.viseon.openOrca.share.PropertyName;
import JsonCodec = ch.viseon.openOrca.share.JsonCodec;
import JsonFactoryJs = ch.viseon.openOrca.client.JsonFactoryJs;
import XMLHttpRequestTransmitter = ch.viseon.openOrca.client.XMLHttpRequestTransmitter;
import ClientOrcaImpl = ch.viseon.openOrca.client.ClientOrcaImpl;
import ClientCommandListExecutor = ch.viseon.openOrca.client.ClientCommandListExecutor;
import PresenttionModelBuilder = ch.viseon.openOrca.share.PresentationModelBuilder
import Source = ch.viseon.openOrca.share.Source;
import LabelPropertyValue = ch.viseon.openOrca.share.LabelPropertyValue;
import PresentationModelBuilder = ch.viseon.openOrca.share.PresentationModelBuilder;
import {Observable} from "rxjs/Observable";
import ClientOrca = ch.viseon.openOrca.client.ClientOrca;
import StringValue = ch.viseon.openOrca.share.StringValue;
import ChangeValueCommandData = ch.viseon.openOrca.share.ChangeValueCommandData;
import ValueChangeEvent = ch.viseon.openOrca.share.ValueChangeEvent;
import ModelStoreChangeEvent = ch.viseon.openOrca.share.ModelStoreChangeEvent;
import PresentationModel = ch.viseon.openOrca.share.PresentationModel;
import ValuePropertyValue = ch.viseon.openOrca.share.ValuePropertyValue;

let modelId: ModelId = new ModelId("Model1");
let modelType = new ModelType("TestModel");
let propertyName = new PropertyName("Prop1");
let rowModelType = new ModelType("rowData");

function createClientOrca(): ClientOrcaImpl {
    let commandListExecutor = new ClientCommandListExecutor();
    let serverUrl = "http://localhost:8080/orca";
    let codec = new JsonCodec(new JsonFactoryJs());
    let transmitter = new XMLHttpRequestTransmitter(
        serverUrl,
        codec
    );
    return new ClientOrcaImpl(commandListExecutor, transmitter);
}


interface ISource {
    DOM: DOMSource
}

function asXStream<T>(obs: Observable<T>): Stream<T> {
    let stream: Stream<T> = xs.create() as Stream<T>;
    obs.subscribe((value) => {
        stream.shamefullySendNext(value)
    });
    return stream;
}

const main = function (source: ISource) {
    let clientOrca: ClientOrca = createClientOrca();

    let inputPm = new PresentationModelBuilder(modelId, modelType, (pmBuilder) => {
        pmBuilder.property_o74rtj$(propertyName, (propertyBuilder) => {
            propertyBuilder.value = new StringValue("0");
        });
    });

    let modelChanges$ = asXStream(
        clientOrca.observeModelById(modelId)
            .map((evt) => evt.valueChangeEvent)
            .map((evt: ValueChangeEvent) => evt.newValue)
            .map((value: StringValue) => value.value)
            .map((newLabel) => p(newLabel))
    )
        .startWith(p("Enter a number"));

    let modelList$ = asXStream(clientOrca.observeModelStore()
        .filter((evt: ModelStoreChangeEvent) => {
            return evt.eventType.isAdd()
        })
        .filter((evt: ModelStoreChangeEvent) => {
            return evt.modelType.stringId == rowModelType.stringId
        })
        .map(evt => {
                let rowModels = clientOrca.getModelByType(rowModelType);
                let liList = rowModels.map((model: PresentationModel) => {
                    return li( (model.getProperties()[0].getValues()[0] as ValuePropertyValue).value)
                });
                return ul(liList)
            }
        )
    )
        .startWith(ul());

    let vDom$ =
        xs.combine(modelChanges$, modelList$)
            .map(([modelChange, modelList]) => {
                return div([
                    h1("An example"),
                    div([
                        input(".noOfModels", {attrs: {type: 'text'}}), 'Enter number of models',
                        modelChange,
                        modelList
                    ])
                ])
            });

    let changeCommands$ = source.DOM
        .select('.noOfModels')
        .events('input')
        .map(ev => (ev.target as HTMLInputElement).value)
        .startWith("")
        .map((value) => new ChangeValueCommandData(Source.REQUEST, modelId, propertyName, new StringValue(value)));

    clientOrca.executeCommands([inputPm.build_9e13n0$(Source.REQUEST)]);

    changeCommands$.addListener({
        next: i => clientOrca.executeCommands([i]),
        error: err => console.error(err),
        complete: () => console.log('completed'),
    });

    return {
        DOM: vDom$
    }
};

run(main, {
    DOM: makeDOMDriver('#main-container'),
});


/**
 * Created by sandro on 11.05.17.
 */

@file:Suppress("UnsafeCastFromDynamic", "NOTHING_TO_INLINE")


@JsModule("xstream")
@JsNonModule
@JsName("xs")
external object xStream {


  @JsName("create")
  fun <T> create(): Stream<T>

}

external class Stream<T>

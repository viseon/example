import ch.viseon.openOrca.client.ClientOrca
import ch.viseon.openOrca.share.*
import kodando.rxjs.Rx
import kodando.rxjs.map

val NAME = ModelId("Hallo")
val PROP_NAME = PropertyName("AProperty")

fun run(orcaSource: ClientOrca): Rx.IObservable<CommandData> {
  val x : Stream<String> = xStream.create<String>()
  return orcaSource.observeModel(NAME)
      .map { ChangeValueCommandData(Source.REQUEST, NAME, PROP_NAME, StringValue("Hallo")) }
}
